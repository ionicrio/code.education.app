/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers').controller('LoginController',
    ['$scope','OAuth','OAuthToken','$cookies','$ionicPopup',
        '$state','UserData','User','$localStorage',
        function ($scope,OAuth,OAuthToken,$cookies,$ionicPopup,
                  $state,UserData,User,$localStorage) {

            $scope.user = {
                username:'',
                password:''
            };

            $scope.login = function () {
                var promise = OAuth.getAccessToken($scope.user);
                promise
                    .then(function (data) {
                        var token = $localStorage.get('device_token');
                        return User.updateDeviceToken({},{device_token:token}).$promise;
                    })
                    .then(function (data) {
                        return User.authenticated({include:'client'}).$promise;
                    })
                    .then(function(data){
                        UserData.set(data.data);
                        if(data.data.role == 'client') {
                            $state.go('client.checkout');
                        }
                        if(data.data.role == 'deliveryman') {
                            $state.go('deliveryman.order');
                        }
                    }, function (resposeError) {
                        console.log(resposeError);
                        UserData.set(null);
                        OAuthToken.removeToken();
                    $ionicPopup.alert({
                        title:'Advertência',
                        template:'Login e/ou senha inválido.',
                    });
                });
            }
        }]);
