/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ClientViewProductController',['$scope','$state','Product','$ionicLoading','$cart','$ionicPopup'
        ,function($scope,$state,Product,$ionicLoading,$cart,$ionicPopup){
        $scope.products = [];

        $ionicLoading.show({
            template:'Carregando...'
        });

        Product.query({},function (data) {
            $scope.products = data.data;
            $ionicLoading.hide();
        },function (dataError) {
            $ionicPopup.alert({
                title:'Advertência',
                template:'Erro na conexão.',
                buttons: [
                    {
                        text: 'Ok',
                        type: 'button-positive',
                        onTap: function(e) {
                            $state.go('login')
                        }
                    }
                ]
            });
            $ionicLoading.hide();
        });


        $scope.addItem = function (item) {
            item.qtd = 1;
            $cart.addItem(item);
            $state.go('client.checkout')
        }

    }]);
