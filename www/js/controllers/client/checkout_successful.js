/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ClientCheckoutSuccessful',['$scope','$state','$cart','$ionicLoading','$ionicPopup',
        function($scope,$state,$cart,$ionicLoading,$ionicPopup){
            var cart = $cart.get();
            $scope.items = cart.items;
            $scope.cupom = cart.cupom;
            $scope.total = $cart.getTotalFinal();
            $cart.clear();
            
            $scope.openListOrder = function () {
                $state.go('client.order');
            }

    }]);
