/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('ClientMenuController',
        ['$scope','$state','$ionicLoading','$ionicPopup','OAuth','User',
        function($scope,$state,$ionicLoading,$ionicPopup,OAuth,User){

            $scope.user = {
                name:''
            };
            
            $ionicLoading.show({
                template:"Carregando..."
            });

            User.authenticated({},function (data) {
                $scope.user = data.data;
                $ionicLoading.hide();
            },function (responseError) {
                $ionicLoading.hide();
            });

    }]);
