/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('DeliverymanMenuController',
        ['$scope','$state','$ionicLoading','$ionicPopup','UserData',
        function($scope,$state,$ionicLoading,$ionicPopup,UserData){
            $scope.user = UserData.get();
        }]);
