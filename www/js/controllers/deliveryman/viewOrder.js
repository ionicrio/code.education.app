/**
 * Created by fabior on 25/04/16.
 */
angular.module('starter.controllers')
    .controller('DeliverymanViewOrderController',['$scope','$stateParams','DeliverymanOrder','$ionicLoading','$ionicPopup','$cordovaGeolocation'
        ,function($scope,$stateParams,DeliverymanOrder,$ionicLoading,$ionicPopup,$cordovaGeolocation){

            var watch,lat = null,long;

            $scope.order = {};

            $ionicLoading.show({
                template:'Carregando...'
            });

            DeliverymanOrder.get({id:$stateParams.id,include:'items,cupom'},function (data) {
                $scope.order = data.data;
                $ionicLoading.hide();

            },function (dataError) {
                $ionicPopup.alert({
                    title:'Advertência',
                    template:'Erro na conexão.',
                });
                $ionicLoading.hide();
            });

            $scope.goToDelivery = function () {
                $ionicPopup.alert({
                    title:'Advertência',
                    template:'Para parar a localização de ok.'
                }).then(function () {
                    stopWatchPosition();
                });

                DeliverymanOrder.updateStatus({id:$stateParams.id},{status:1},function () {
                    //geo
                    var watchOptions = {
                        timeout: 3000,
                        enableHighAccuracy: false
                    }
                    watch = $cordovaGeolocation.watchPosition(watchOptions);
                    watch.then(null,
                        function (responseError) {
                            //error
                            console.log(responseError);
                        },function (position) {
                            if(!lat){
                                lat = position.coords.latitude;
                                long = position.coords.longitude;
                            }else{
                                long -= 0.0444;
                            }
                            DeliverymanOrder.geo({id:$stateParams.id},
                                {
                                    lat:lat,
                                    long:long
                                });
                        });
               });
            };

            function stopWatchPosition() {
                if(watch && typeof watch == 'object' && watch.hasOwnProperty('watchID')){
                    $cordovaGeolocation.clearWatch(watch.watchID);
                }
            }



    }]);
