// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires' x
angular.module('starter.controllers',[]);
angular.module('starter.services',[]);
angular.module('starter.filters',[]);
angular.module('starter', ['ionic','ionic.service.core',
                            'starter.controllers',
                            'starter.services',
                            'starter.filters',
                            'angular-oauth2',
                            'pusher-angular',
                            'ngResource',
                            'uiGmapgoogle-maps',
                            'ngCordova'])
    .constant('appConfig',{
        OAuth:{
            baseUrl: 'http://codeeducation.fabiofarias.com.br',
            clientId: 'appid01',
            clientSecret: 'secret',
            grantPath:'/oauth/access_token'
        },
        baseUrl: 'http://codeeducation.fabiofarias.com.br',
        pusherKey: '754be20ced40379f6755',
        pusherId: '205233',
    })
    .run(function($ionicPlatform,$window,appConfig,$localStorage) {

    $window.client = new Pusher(appConfig.pusherKey);

    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
          // for form inputs)
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

          // Don't remove this line unless you know what you are doing. It stops the viewport
          // from snapping when text inputs are focused. Ionic handles this internally for
          // a much nicer keyboard experience.
          cordova.plugins.Keyboard.disableScroll(true);
        }
        if(window.StatusBar) {
          StatusBar.styleDefault();
        }
        Ionic.io();
        var push = new Ionic.Push({
            debug: true,
            onNotification: function(notification) {
                var payload = notification.payload;
                console.log(notification, payload);
            },
            onRegister: function(data) {
                console.log(data.token);
            },
            pluginConfig: {
                "ios": {
                    "badge": true,
                    "sound": true
                },
                "android": {
                    "iconColor": "#343434"
                }
            }
        });

        push.register(function(token) {
            $localStorage.set('device_token',token.token);
            console.log("Device token:",token.token);
            push.saveToken(token);  // persist the token in the Ionic Platform
        });
  });

}).config(function ($stateProvider,$urlRouterProvider,OAuthProvider,
                    OAuthTokenProvider,appConfig,$provide) {

    OAuthProvider.configure(appConfig.OAuth);


    OAuthTokenProvider.configure({
        name: 'token',
        options: {
            secure: false
        }
    });
    $stateProvider
        .state('login',{
            url:'/login',
            templateUrl:'templates/login.html',
            controller: 'LoginController'
        })
        .state('client',{
            abstract:true,
            cache:false,
            url:'/client',
            templateUrl:'templates/client/menu.html', 
            controller:'ClientMenuController'
        })
        .state('client.order',{
            cache:false,
            url:'/order',
            templateUrl:'templates/client/order.html',
            controller:'ClientOrderController'
        })
        .state('client.view_order',{
            cache:false,
            url:'/view_order/:id',
            templateUrl:'templates/client/view_order.html',
            controller:'ClientViewOrderController'
        })
        .state('client.checkout',{
            cache:false,
            url:'/checkout',
            templateUrl:'templates/client/checkout.html',
            controller:'ClientCheckoutController'
        })
        .state('client.checkout_item_datail',{
            url:'/checkout/detail/:index',
            templateUrl:'templates/client/checkout_item_detail.html',
            controller:'ClientCheckoutDetailController'
        })
        .state('client.checkout_successful',{
            cache:false,
            url:'/checkout_succesful',
            templateUrl:'templates/client/checkout_successful.html',
            controller:'ClientCheckoutSuccessful'
        })
        .state('client.view_products',{
            url:'/view_products',
            templateUrl:'templates/client/view_products.html',
            controller:'ClientViewProductController'
        })
        .state('client.view_delivery',{
            cache:false,
            url:'/view_delivery/:id',
            templateUrl:'templates/client/view_delivery.html',
            controller:'ClientViewDeliveryController'
        })
        .state('deliveryman',{
            abstract:true,
            cache:false,
            url:'/deliveryman',
            templateUrl:'templates/deliveryman/menu.html',
            controller:'DeliverymanMenuController'
        })
        .state('deliveryman.order',{
            url:'/order',
            templateUrl:'templates/deliveryman/order.html',
            controller:'DeliverymanOrderController'
        })
        .state('deliveryman.view_order',{
            cache:false,
            url:'/view_order/:id',
            templateUrl:'templates/deliveryman/view_order.html',
            controller:'DeliverymanViewOrderController'
        });

    $urlRouterProvider.otherwise('/login');

    $provide.decorator('OAuthToken',['$localStorage','$delegate',function ($localStorage,$delegate) {
        Object.defineProperties($delegate,{
           setToken:{
                value:function (data) {
                    return $localStorage.setObject('token',data);
                },
               enumerable: true,
               configurable:true,
               writable:true
           },
           getToken:{
               value:function () {
                   return $localStorage.getObject('token');
               },
               enumerable: true,
               configurable:true,
               writable:true
           },
           removeToken:{
               value:function () {
                   $localStorage.setObject('token',null);
               },
               enumerable: true,
               configurable:true,
               writable:true
           }
        });

        return $delegate;
    }]);
});
